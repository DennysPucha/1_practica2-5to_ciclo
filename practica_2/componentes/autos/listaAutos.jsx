import { obtener } from "@/hooks/Conexion";

export async function ListaAutos() {
  const response = await obtener('index.php?funcion=marcas');
  console.log(response);

  if (response.code === 200 && Array.isArray(response.datos)) {
    return (
      <div>
        <div className="container">
          {response.datos.map((marca, i) => (
            <div key={i} className="list-group">
              <div href="#" className="list-group-item list-group-item-action active" style={{ margin: "2px" }} aria-current="true">
                <div className="d-flex w-100 justify-content-between">
                  <h5 className="mb-1">{marca.nombre || 'Sin nombre'}</h5>
                  <small>Estado: {marca.estado}</small>
                </div>
                <p className="mb-1">ID: {marca.external_id}</p>
              </div>
            </div>
          ))}
        </div>
      </div>

    );
  }
}
