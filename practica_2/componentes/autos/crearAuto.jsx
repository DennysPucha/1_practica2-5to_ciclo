"use client";
import { getToken, getExternalUser} from "@/hooks/SessionUtil";
import { useState,useEffect } from "react";
import { crearAuto } from "@/hooks/Conexion";
const datosQuemados = {
  descripcion: "Descripción del auto",
  subtotal: "1000.0",
  iva: "200.0",
  total: "1200.0",
  descuento: "50.0",
  placa: "ABC123",
  chasis: "Chasis123",
  foto: "http://auto.com/images/a.png",
  funcion: "guardarAuto"
};

const recurso = "index.php";

const CrearAutoConToken = () => {
  const [autos, setAutos] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const token = getToken();
      const externalUser = getExternalUser();
      const marca="9843b3be-833d-11ee-a1d4-581122836c5f";
      if (token) {

          try {
            const resultado = await crearAuto(recurso, datosQuemados, token, externalUser, marca);
            
            const resultadoH1 = document.createElement("h1");
            resultadoH1.textContent = `Respuesta del servidor: ${JSON.stringify(resultado)}`;
            document.body.appendChild(resultadoH1);
          } catch (error) {
            console.error("Error al enviar la solicitud:", error);
          }

      }
    };

    fetchData();
  }, []);

  return (
    <div>

    </div>
  );
};

export default CrearAutoConToken;
