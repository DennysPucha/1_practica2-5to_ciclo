"use client";
import React, { useEffect, useState } from "react";
import { getExternalUser, getToken } from "@/hooks/SessionUtil";
import { obtenerAutos } from "@/hooks/Conexion";
import Link from "next/link";

const ListarAutos = () => {
  const [autos, setAutos] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const token = getToken();
      const externalUser = getExternalUser();

      if (token) {
        try {
          const response = await obtenerAutos(`index.php?funcion=listar_auto_user&external=${externalUser}`, token);
          setAutos(response.datos);
          console.log(response);
        } catch (error) {
          console.log(error);
        }
      }
    };

    fetchData();
  }, []);

  return (
    <div>
      {Array.isArray(autos) && autos.length > 0 ? (
        <div>
          <table className="table">
            <thead>
              <tr>
                <th>Placa</th>
                <th>Chasis</th>
                <th>Descripción</th>
                <th>Descuento</th>
                <th>External</th>
                <th>Foto</th>
                <th>IVA</th>
                <th>Marca External</th>
                <th>Nombre</th>
                <th>Subtotal</th>
                <th>Total</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
              {autos.map((auto, index) => (
                <tr key={index}>
                  <td>{auto.placa}</td>
                  <td>{auto.chasis}</td>
                  <td>{auto.descripcion}</td>
                  <td>{auto.descuento}</td>
                  <td>{auto.external}</td>
                  <td>
                    <img src={auto.foto} style={{ maxWidth: "100px" }} />
                  </td>
                  <td>{auto.iva}</td>
                  <td>{auto.marca_external}</td>
                  <td>{auto.nombre}</td>
                  <td>{auto.subtotal}</td>
                  <td>{auto.total}</td>
                  <td>
                    {auto.external && (
                      <Link href={`editar/${auto.external}`} passHref>
                        <button className="btn btn-success">
                          Editar
                        </button>
                      </Link>
                    )}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      ) : (
        <p>No se encontraron autos.</p>
      )}
    </div>
  );
};

export default ListarAutos;
