"use client";
import { obtener } from "@/hooks/Conexion";
import Select from 'react-select';

export async function LlenarMarcas() {
  const response = await obtener('index.php?funcion=marcas');
  console.log(response);

  if (response.code === 200 && Array.isArray(response.datos)) {
    const options = response.datos.map((marca, i) => ({
      value: marca.external_id,
      label: marca.nombre || 'Sin nombre',
    }));

    return (
      <div>
        <div className="container">
          <Select options={options} />
        </div>
      </div>
    );
  }
}
