import React from "react";
const FormularioAutos = () => {
    return (
        <div className="container mt-5">
            <div className="row justify-content-center">
                <div className="col-md-6">
                    <form>
                        <div className="mb-3">
                            <label htmlFor="descripcion" className="form-label">Descripción:</label>
                            <textarea className="form-control" id="descripcion" rows="3" placeholder="Ingrese alguna descripción"></textarea>
                        </div>

                        <div className="mb-3">
                            <label htmlFor="subtotal" className="form-label">Subtotal:</label>
                            <input type="text" className="form-control" id="subtotal" placeholder="0.0" />
                        </div>

                        <div className="mb-3">
                            <label htmlFor="iva" className="form-label">IVA:</label>
                            <input type="text" className="form-control" id="iva" placeholder="0.0" />
                        </div>

                        <div className="mb-3">
                            <label htmlFor="total" className="form-label">Total:</label>
                            <input type="text" className="form-control" id="total" placeholder="0.0" />
                        </div>

                        <div className="mb-3">
                            <label htmlFor="descuento" className="form-label">Descuento:</label>
                            <input type="text" className="form-control" id="descuento" placeholder="0.0" />
                        </div>

                        <div className="mb-3">
                            <label htmlFor="placa" className="form-label">Placa del Auto:</label>
                            <input type="text" className="form-control" id="placa" placeholder="Ingrese la placa del auto" />
                        </div>

                        <div className="mb-3">
                            <label htmlFor="chasis" className="form-label">Chasis:</label>
                            <input type="text" className="form-control" id="chasis" placeholder="El chasis debe ser único" />
                        </div>

                        <div className="mb-3">
                            <label htmlFor="foto" className="form-label">Foto (URL):</label>
                            <input type="text" className="form-control" id="foto" placeholder="http://auto.com/images/a.png" />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="marca" className="form-label">Marca:</label>
                            <select className="form-select" id="marca">
                                <option value="marca1">Marca 1</option>
                                <option value="marca2">Marca 2</option>
                                <option value="marca3">Marca 3</option>
                            </select>
                        </div>
                        <button type="submit" className="btn btn-primary">Enviar</button>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default FormularioAutos;
