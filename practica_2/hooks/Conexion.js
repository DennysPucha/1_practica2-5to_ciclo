let URL = "https://computacion.unl.edu.ec/pdml/practica1/";



export function url_api(){
    return URL;
}

export function obtener(recurso) {
    return fetch(URL + recurso)
        .then(response => response.json());
}

export async function obtenerAutos(recurso,token) {
    const headers = {
      Accept: "application/json",
      "content-type":"application/json",
      "TOKEN-KEY": token,
    };
    
    const response = await fetch(URL + recurso, {
      method: "GET",
      headers: headers,
      cache:'no-store',
    });
    return await response.json();
  }


export async function enviar(recurso,data){
    const headers={
        "Accept": "application/json",
    };
    const response =await fetch(URL+recurso,{
        method:"POST",
        headers:headers,
        body:JSON.stringify(data)
    });
    return await response.json();
}

export async function crearAuto(data, token, userId, marcaid) {
  const headers = {
    Accept: "application/json",
    "Content-Type": "application/json",
    "TOKEN-KEY": token,
  };

  const datosConUserIdMarcaId = {
    ...data,
    user: userId,
    marca: marcaid,
  };

  const response = await fetch(URL + "index.php", {
    method: "POST",
    headers: headers,
    body: JSON.stringify(datosConUserIdMarcaId),
  });

  return await response.json();
}


export async function modificarAuto(data, token, marcaid,idAuto) {
  const headers = {
    Accept: "application/json",
    "Content-Type": "application/json",
    "TOKEN-KEY": token,
  };

  const datosConUserIdMarcaId = {
    ...data,
    marca: marcaid,
    external:idAuto
  };

  const response = await fetch(URL + "index.php", {
    method: "POST",
    headers: headers,
    body: JSON.stringify(datosConUserIdMarcaId),
  });

  return await response.json();
}