import Image from 'next/image'
import { ListaAutos } from '@/componentes/autos/listaAutos';
import Menu from '@/componentes/menu';
export default function Home() {
  return (    
      <div className="container">
      <header>
        <Menu />
      </header>
        <h1>Marcas disponibles</h1>
        <div className="container-fluid">
          <ListaAutos />

        </div>
      </div>
  )
}
