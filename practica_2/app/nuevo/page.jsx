"use client";
import MenuAdminEdit from "@/componentes/menuAdminEdit";
import * as Yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import { estaSesion } from '@/hooks/SessionUtil';
import { useRouter } from 'next/navigation'
import React from 'react';
import { crearAuto } from "@/hooks/Conexion";
import { getToken,getExternalUser } from "@/hooks/SessionUtil";
import { useState } from "react";


export default function nuevo() {
    const [selectedMarca, setSelectedMarca] = useState("");
    const ErrorCard = ({ errors }) => {
        return (
            <div className="error-card">
                <h4>Errores:</h4>
                <ul>
                    {Object.keys(errors).map((key, index) => (
                        <li key={index}>{errors[key]?.message}</li>
                    ))}
                </ul>
            </div>
        );
    };
    const router = useRouter();


    const validationSchema = Yup.object().shape({
        descripcion: Yup.string().required('Ingrese alguna descripción'),
        subtotal: Yup.string().required('Ingrese el subtotal'),
        iva: Yup.string().required('Ingrese el IVA'),
        total: Yup.string().required('Ingrese el total'),
        descuento: Yup.string().required('Ingrese el descuento'),
        placa: Yup.string().required('Ingrese la placa del auto'),
        chasis: Yup.string().required('Ingrese el chasis del auto'),
        foto: Yup.string().url('Ingrese una URL válida').required('Ingrese la URL de la foto'),
    });

    const formOptions = { resolver: yupResolver(validationSchema) };
    const { register, handleSubmit, formState } = useForm(formOptions);
    const { errors } = formState;

    const handleMarcaChange = (e) => {
        setSelectedMarca(e.target.value);
    };

    const sendData = async (data) => {
        const token = getToken();
        const externalUser = getExternalUser();
        const marca = selectedMarca;

        if (estaSesion()) {
            try {
                var data = {
                    "descripcion": data.descripcion,
                    "subtotal": data.subtotal,
                    "iva": data.iva,
                    "total": data.total,
                    "descuento": data.descuento,
                    "placa": data.placa,
                    "chasis": data.chasis,
                    "foto": data.foto,
                    "funcion": "guardarAuto"
                };
                await crearAuto(data, token, externalUser, marca);
                router.push("/principal");
            } catch (error) {
                console.error("Error al enviar la solicitud:", error);
            }
        }
    };




    return (
        <div>
            <MenuAdminEdit></MenuAdminEdit>
            <div className="container">
                <div className="container mt-5">
                    <div className="row justify-content-center">
                        <div className="col-md-6">
                            <form onSubmit={handleSubmit(sendData)}>
                                <div className="mb-3">
                                    <label htmlFor="descripcion" className="form-label">Descripción:</label>
                                    <textarea {...register('descripcion')} className="form-control" id="descripcion" rows="3" placeholder="Ingrese alguna descripción"></textarea>
                                </div>

                                <div className="mb-3">
                                    <label htmlFor="subtotal" className="form-label">Subtotal:</label>
                                    <input {...register('subtotal')} type="text" className="form-control" id="subtotal" placeholder="0.0" />
                                </div>

                                <div className="mb-3">
                                    <label htmlFor="iva" className="form-label">IVA:</label>
                                    <input {...register('iva')} type="text" className="form-control" id="iva" placeholder="0.0" />
                                </div>

                                <div className="mb-3">
                                    <label htmlFor="total" className="form-label">Total:</label>
                                    <input {...register('total')} type="text" className="form-control" id="total" placeholder="0.0" />
                                </div>

                                <div className="mb-3">
                                    <label htmlFor="descuento" className="form-label">Descuento:</label>
                                    <input {...register('descuento')} type="text" className="form-control" id="descuento" placeholder="0.0" />
                                </div>

                                <div className="mb-3">
                                    <label htmlFor="placa" className="form-label">Placa del Auto:</label>
                                    <input {...register('placa')} type="text" className="form-control" id="placa" placeholder="Ingrese la placa del auto" />
                                </div>

                                <div className="mb-3">
                                    <label htmlFor="chasis" className="form-label">Chasis:</label>
                                    <input {...register('chasis')} type="text" className="form-control" id="chasis" placeholder="El chasis debe ser único" />
                                </div>

                                <div className="mb-3">
                                    <label htmlFor="foto" className="form-label">Foto (URL):</label>
                                    <input {...register('foto')} type="text" className="form-control" id="foto" placeholder="http://auto.com/images/a.png" />
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="marca" className="form-label">Marca:</label>
                                    <select className="form-select" id="marca" onChange={handleMarcaChange} value={selectedMarca}>
                                        <option value="9843b3be-833d-11ee-a1d4-581122836c5f">Toyota</option>
                                        <option value="9843b6db-833d-11ee-a1d4-581122836c5f">Nissan</option>
                                        <option value="9843b7d4-833d-11ee-a1d4-581122836c5f">Chevrolet</option>
                                        <option value="9843b81f-833d-11ee-a1d4-581122836c5f">Mitsubishi</option>
                                        <option value="9843b866-833d-11ee-a1d4-581122836c5f">Audi</option>
                                    </select>
                                </div>
                                <button type="submit" className="btn btn-primary">Enviar</button>
                            </form>


                        </div>
                    </div>
                </div>
            </div>
            <div className="col-md-3">
                {Object.keys(errors).length > 0 && <ErrorCard errors={errors} />}
            </div>
        </div>
    );
}