"use Client";
import ListarAutos from "@/componentes/autos/listarAutos";
import MenuAdmin from "@/componentes/menuAdmin";
import Link from "next/link";

export default function principal() {
  return (
    <div className="container">
      <MenuAdmin></MenuAdmin>
      <div className="submenu" style={{margin:"10px"}}>
      <Link type="button" href="/nuevo" className="btn btn-success">Nuevo Auto</Link>
      </div>
      <div className="container-fluid">
        <ListarAutos></ListarAutos>
      </div>
    </div>
  );
}
